**O(objective)**:

Learn Spring Boot Integration Test.Use MockMvc to mock a request, so that we can test our controller.Learn how to use mock to mock a repository, give us the result we want repository give.Then we can focus on the business logic and ignore DAO.

Learn Spring Boot Architecture.When our project is complicated, we need to divided our project to 3 layer including DAO,service,controller. DAO use to crud data, service use to impletement business logic, controller use to handle request.

Learn how to use @RestControllerAdvice,@ExceptionHandler to help us return status code when our controller throw a exception
<br><br>**R(Reflective)**:Little bit Hard
<br><br>**I(Interpretive)**:Mock is a very useful tool. When our DAO is not completed, we can use mock to make DAO return the result we want.So that we can test out service. And it can help us avoid running server while testing.
<br><br>**D(Decisional)**:Practice more in using mock.Some testing tools have many useful method and properties, I need to know more about that.