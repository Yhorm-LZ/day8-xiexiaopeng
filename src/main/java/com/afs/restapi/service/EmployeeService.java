package com.afs.restapi.service;

import com.afs.restapi.exception.*;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.pojo.Employee;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        Employee employee = employeeRepository.findById(id);
        if(employee==null){
            throw new EmployeeNotFoundException();
        }
        return employee;
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeRepository.findAll().stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Employee insert(Employee newEmployee) {
        if(!newEmployee.IsValidAge()){
            throw new InvalidAgeException();
        }
        if(newEmployee.isInvalidAgeAndSalary()){
            throw new SalaryNotMatchAgeException();
        }
        newEmployee.setActivate(true);
        return employeeRepository.insert(newEmployee);
    }

    public Employee update(Long id, Employee employee) {
        Employee updateEmployee=findById(id);
        if(updateEmployee==null){
            throw new NotFoundException();
        }
        if(!updateEmployee.getActivate()){
            throw new UpdateInactivateEmployeeException();
        }
        updateEmployee.merge(employee);
        return employeeRepository.update(updateEmployee);
    }

    public void delete(Long id) {
        Employee deleteEmployee = employeeRepository.findById(id);
        if(deleteEmployee==null){
            throw new NotFoundException();
        }
        employeeRepository.delete(deleteEmployee);
    }

}
