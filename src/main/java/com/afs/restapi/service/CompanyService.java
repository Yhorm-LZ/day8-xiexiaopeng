package com.afs.restapi.service;

import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.exception.UpdateInactivateEmployeeException;
import com.afs.restapi.pojo.Company;
import com.afs.restapi.pojo.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    @Autowired
    CompanyRepository companyRepository;

    public List<Company> getCompanies(){
        return companyRepository.getCompanies();
    }

    public Company getCompaniesById(Long id){
        return companyRepository.getCompaniesById(id);
    }

    public List<Employee> getEmployeesByCompanyId(Long id){
        return companyRepository.getEmployeesByCompanyId(id);
    }

    public List<Company> getCompaniesByPage(Integer page, Integer size){
        return companyRepository.getCompaniesByPage(page,size);
    }

    public Company addCompany(Company company){
        return companyRepository.addCompany(company);
    }

    public void updateCompany(Long id,Company company){
        Company updateCompany=getCompaniesById(id);
        if(updateCompany==null){
            throw new NotFoundException();
        }
        updateCompany.setName(company.getName());
        companyRepository.updateCompany(updateCompany);
    }

    public void deleteCompany(Long id){
        Company deleteCompany= companyRepository.getCompaniesById(id);
        if(deleteCompany==null){
            throw new NotFoundException();
        }
        companyRepository.deleteCompany(deleteCompany);
    }
}
