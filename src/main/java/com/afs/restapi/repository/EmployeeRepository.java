package com.afs.restapi.repository;

import com.afs.restapi.pojo.Employee;
import com.afs.restapi.exception.NotFoundException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private final List<Employee> employees = new ArrayList<>();
    private final AtomicLong atomicID = new AtomicLong(5L);

    public EmployeeRepository() {
        employees.add(new Employee(1L, "John Smith", 32, "Male", 5000.0));
        employees.add(new Employee(2L, "Jane Johnson", 28, "Female", 6000.0));
        employees.add(new Employee(3L, "David Williams", 35,"Male", 5500.0));
        employees.add(new Employee(4L, "Emily Brown", 23, "Female", 4500.0));
        employees.add(new Employee(5L, "Michael Jones", 40, "Male", 7000.0));
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    public List<Employee> findByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }


    public Employee insert(Employee newEmployee) {
        newEmployee.setId(atomicID.incrementAndGet());
        newEmployee.setActivate(true);
        employees.add(newEmployee);
        return newEmployee;
    }

    public Employee update(Employee updateEmployeeData) {
        Employee updateEmployee = employees.stream().filter(employee -> Objects.equals(updateEmployeeData.getId(), employee.getId())).findFirst().orElseThrow(NotFoundException::new);
        updateEmployee.setId(updateEmployeeData.getId());
        updateEmployee.setAge(updateEmployeeData.getAge());
        updateEmployee.setName(updateEmployeeData.getName());
        updateEmployee.setGender(updateEmployeeData.getGender());
        updateEmployee.setSalary(updateEmployeeData.getSalary());
        return updateEmployee;
    }

    public void delete(Employee deleteEmployee) {
        Employee toDeleteEmployee = employees.stream().filter(employee -> Objects.equals(deleteEmployee.getId(), employee.getId())).findFirst().get();
        toDeleteEmployee.setActivate(false);
    }

    public void clearAll() {
        employees.clear();
    }
}

