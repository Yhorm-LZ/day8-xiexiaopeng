package com.afs.restapi.repository;

import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.pojo.Company;
import com.afs.restapi.pojo.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    @Resource
    private EmployeeRepository employeeRepository;
    private final List<Company> companies = new ArrayList<>();

    private static final AtomicLong atomicId = new AtomicLong(0);

    public CompanyRepository() {
        companies.add(new Company(1L,"huawei"));
        companies.add(new Company(2L,"xiaomi"));
    }

    public List<Company> getCompanies(){
        return companies;
    }

    public Company getCompaniesById(Long id){
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst().orElse(null);
    }

    public List<Employee> getEmployeesByCompanyId(Long id){
        return employeeRepository.findAll().stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(),id))
                .collect(Collectors.toList());
    }

    public List<Company> getCompaniesByPage(Integer page, Integer size){
        return companies.stream()
                .skip((long) (page-1) *size).limit(size)
                .collect(Collectors.toList());
    }

    public Company addCompany(Company company){
        company.setId(atomicId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public void updateCompany(Company updateCompany){
        Company toUpdateCompany = companies.stream().filter(company -> Objects.equals(updateCompany.getId(), company.getId())).findFirst().orElseThrow(NotFoundException::new);
        toUpdateCompany.setId(updateCompany.getId());
        toUpdateCompany.setName(updateCompany.getName());
    }

    public void deleteCompany(Company deleteCompany){
        Company toDeleteCompany = companies.stream().filter(company -> Objects.equals(deleteCompany.getId(), company.getId())).findFirst().get();
        companies.remove(toDeleteCompany);
    }

    public void clearAll() {
        companies.clear();
    }
}
