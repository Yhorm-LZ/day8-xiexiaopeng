package com.afs.restapi.controller;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.pojo.Company;
import com.afs.restapi.pojo.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.service.CompanyService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    @Resource
    private CompanyService companyService;
    @GetMapping
    public List<Company> getCompanies(){
        return companyService.getCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompaniesById(@PathVariable Long id){
        Company company=companyService.getCompaniesById(id);
        if(company==null){
            throw new CompanyNotFoundException();
        }
        return company;
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id){
        return companyService.getEmployeesByCompanyId(id);
    }

    @GetMapping(params = {"page","size"})
    public List<Company> getCompaniesByPage(@RequestParam("page")Integer page, @RequestParam("size") Integer size){
        return companyService.getCompaniesByPage(page,size);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company){
        return companyService.addCompany(company);
    }

    @PutMapping("/{id}")
    public void updateCompany(@PathVariable Long id,@RequestBody Company company){
        companyService.updateCompany(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id){
        companyService.deleteCompany(id);
    }
}
