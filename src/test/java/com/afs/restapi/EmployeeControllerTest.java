package com.afs.restapi;

import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.pojo.Employee;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    MockMvc client;

    @BeforeEach
    void cleanAll(){
        employeeRepository.clearAll();
    }

    @Test
    void should_return_employees_when_getAllEmployees_given_employees() throws Exception {

        Employee john = new Employee(1L, "Lily1", 20, "Female", 8000D);
        employeeRepository.insert(john);

        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[0].age").value(john.getAge()))
                .andExpect(jsonPath("$[0].gender").value(john.getGender()))
                .andExpect(jsonPath("$[0].salary").value(john.getSalary()));

    }

    @Test
    void should_return_employees_with_gender_when_perform_findEmployeeByGender_given_two_employees_with_different_gender() throws Exception {

        Employee john = new Employee(1L, "Jhon", 20, "Female", 8000D);
        Employee emily = new Employee(2L, "Emily", 20, "Female", 8000D);
        employeeRepository.insert(john);
        employeeRepository.insert(emily);

        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "Female"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[0].age").value(john.getAge()))
                .andExpect(jsonPath("$[0].gender").value(john.getGender()))
                .andExpect(jsonPath("$[0].salary").value(john.getSalary()));
    }

    @Test
    void should_return_created_employee_when_perform_insertEmployee_given_employee_json() throws Exception {
        Employee john = new Employee(null, "john", 22, "Male", 8000D);
        String johnJson = new ObjectMapper().writeValueAsString(john);

        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("john"))
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").value("Male"))
                .andExpect(jsonPath("$.salary").value(8000.0))
                .andExpect(jsonPath("$.activate").value(true));
    }

    @Test
    void should_return_update_employee_when_perform_updateEmployee_given_employee_json() throws Exception {
        Employee john = new Employee(null, "john", 22, "Male", 8000D);
        employeeRepository.insert(john);
        Employee newJohn = new Employee(null, null, 30, null, 10000D);
        String johnJson = new ObjectMapper().writeValueAsString(newJohn);
        client.perform(MockMvcRequestBuilders.put("/employees/"+ john.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.age").value(newJohn.getAge()))
                .andExpect(jsonPath("$.salary").value(newJohn.getSalary()));
    }

    @Test
    void should_return_no_content_when_perform_deleteEmployee_given_id() throws Exception {
        Employee john = new Employee(null, "john", 22, "Male", 8000D);
        employeeRepository.insert(john);
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", john.getId()))
                .andExpect(status().isNoContent());
        Assertions.assertFalse(john.getActivate());
    }

    @Test
    void should_return_employees_when_perform_findByPage_given_page_and_size() throws Exception {
        Employee john1 = new Employee(null, "john1", 22, "Male", 8000D);
        Employee john2 = new Employee(null, "john2", 22, "Male", 8000D);
        Employee john3 = new Employee(null, "john3", 22, "Male", 8000D);
        Employee john4 = new Employee(null, "john4", 22, "Male", 8000D);
        Employee john5 = new Employee(null, "john5", 22, "Male", 8000D);
        Employee john6 = new Employee(null, "john6", 22, "Male", 8000D);
        employeeRepository.insert(john1);
        employeeRepository.insert(john2);
        employeeRepository.insert(john3);
        employeeRepository.insert(john4);
        employeeRepository.insert(john5);
        employeeRepository.insert(john6);
        client.perform(MockMvcRequestBuilders.get("/employees").param("page", "1").param("size", "5"))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].name").value(john1.getName()));
    }

    @Test
    void should_return_bad_request_when_perform_findEmployeeById_given_nonexistent_id() throws Exception {

        Employee john = new Employee(1L, "Jhon", 20, "Female", 8000D);
        Employee emily = new Employee(2L, "Emily", 20, "Female", 8000D);
        employeeRepository.insert(john);
        employeeRepository.insert(emily);

        client.perform(MockMvcRequestBuilders.get("/employees/{id}",120))
                .andExpect(status().isBadRequest());
    }
}
