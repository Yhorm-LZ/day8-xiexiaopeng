package com.afs.restapi;


import com.afs.restapi.pojo.Company;
import com.afs.restapi.pojo.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    MockMvc client;

    @BeforeEach
    void cleanAll(){
        companyRepository.clearAll();
    }

    @Test
    void should_return_companies_when_perform_getCompanies_given_companies() throws Exception {
        Company xiaomi=new Company(null,"xiaomi");
        companyRepository.addCompany(xiaomi);

        client.perform(MockMvcRequestBuilders.get("/companies/{id}",xiaomi.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(xiaomi.getId()))
                .andExpect(jsonPath("$.name").value(xiaomi.getName()));
    }

    @Test
    void should_return_company_when_perform_getCompaniesById_given_company_id() throws Exception {

        Company xiaomi=new Company(null,"xiaomi");
        companyRepository.addCompany(xiaomi);

        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(xiaomi.getId()))
                .andExpect(jsonPath("$[0].name").value(xiaomi.getName()));
    }

    @Test
    void should_return_no_content_when_perform_deleteCompany_given_id() throws Exception {
        Company company = new Company(null, "huawei");
        companyRepository.addCompany(company);
        client.perform(MockMvcRequestBuilders.delete("/companies/{id}", company.getId()))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_success_code_when_perform_updateEmployee_given_employee_json() throws Exception {
        Company company = new Company(null, "huawei");
        companyRepository.addCompany(company);
        Company newCompany = new Company(null,"xiaomi");
        String johnJson = new ObjectMapper().writeValueAsString(newCompany);
        client.perform(MockMvcRequestBuilders.put("/companies/"+ company.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnJson))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_created_company_when_perform_addCompany_given_company_json() throws Exception {
        Company company = new Company(null, "bytedance");
        String johnJson = new ObjectMapper().writeValueAsString(company);
        client.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("bytedance"));
    }

    @Test
    void should_return_companies_when_perform_getCompaniesByPage_given_page_and_size() throws Exception {
        Company huawei = new Company(null, "huawei");
        Company xiaomi = new Company(null, "xiaomi");
        Company bytedance = new Company(null, "bytedance");
        Company google = new Company(null, "google");
        companyRepository.addCompany(huawei);
        companyRepository.addCompany(xiaomi);
        companyRepository.addCompany(bytedance);
        companyRepository.addCompany(google);

        client.perform(MockMvcRequestBuilders.get("/companies").param("page", "1").param("size", "2"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(huawei.getId()))
                .andExpect(jsonPath("$[0].name").value(huawei.getName()))
                .andExpect(jsonPath("$[1].id").value(xiaomi.getId()))
                .andExpect(jsonPath("$[1].name").value(xiaomi.getName()));
    }

    @Test
    void should_return_bad_request_when_perform_getCompaniesById_given_nonexistent_id() throws Exception {

        Company huawei = new Company(1L, "huawei");
        Company xiaomi = new Company(2L, "xiaomi");
        companyRepository.addCompany(huawei);
        companyRepository.addCompany(xiaomi);

        client.perform(MockMvcRequestBuilders.get("/companies/{id}",120))
                .andExpect(status().isBadRequest());
    }

}
