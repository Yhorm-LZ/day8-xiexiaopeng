package com.afs.restapi;

import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.SalaryNotMatchAgeException;
import com.afs.restapi.exception.InvalidAgeException;
import com.afs.restapi.exception.UpdateInactivateEmployeeException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import com.afs.restapi.pojo.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {
    private static final EmployeeRepository employeeRepositoryMock=mock(EmployeeRepository.class);

    @Test
    void should_not_create_when_insert_given_employee_with_invalid_age(){
        EmployeeService employeeService=new EmployeeService(employeeRepositoryMock);

        Employee employee = new Employee(1L, "name", 15, "male", 6000D);

        Assertions.assertThrows(InvalidAgeException.class,()->employeeService.insert(employee));
    }

    @Test
    void should_not_create_when_insert_given_employee_with_age_greater_than_30_while_salary_less_than_20000(){
        EmployeeService employeeService=new EmployeeService(employeeRepositoryMock);

        Employee employee = new Employee(1L, "name", 45, "male", 10000D);

        Assertions.assertThrows(SalaryNotMatchAgeException.class,()->employeeService.insert(employee));
    }

    @Test
    void should_throw_employee_not_found_exception_when_findById_given_invalid_employee_id(){
        EmployeeService employeeService=new EmployeeService(employeeRepositoryMock);
        given(employeeRepositoryMock.findById(1L)).willReturn(null);
        Assertions.assertThrows(EmployeeNotFoundException.class,()->employeeService.findById(1L));
    }

    @Test
    void should_set_inactivate_when_delete_given_active_Employee(){
        EmployeeService employeeService=new EmployeeService(employeeRepositoryMock);
        Employee employee=new Employee(1L,"name",30,"male",25000D);
        employee.setActivate(true);
        given(employeeRepositoryMock.findById(1L)).willReturn(employee);

        employeeService.delete(1L);

        verify(employeeRepositoryMock,times(1)).delete(employee);

    }

    @Test
    void should_not_update_when_update_given_inactive_Employee(){
        EmployeeService employeeService=new EmployeeService(employeeRepositoryMock);
        Employee employee=new Employee(1L,"name",30,"male",25000D);
        employee.setActivate(false);
        given(employeeRepositoryMock.findById(1L)).willReturn(employee);

        Assertions.assertThrows(UpdateInactivateEmployeeException.class,()->employeeService.update(1L,employee));

    }
}
